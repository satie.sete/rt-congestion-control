# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 11:24:39 2020

@author: Guenole CHEROT
"""
import pandapower as pp
import pandapower.networks
import warnings
warnings.filterwarnings("ignore")

from rt_congestion_control.so import SO
from rt_congestion_control.communication import Communication
from rt_congestion_control.admm import Admm

import numpy as np

import os
absolute_dirpath = os.path.abspath(os.path.dirname(__file__))

def import_net(begin=1150, end=1200, rho=700, flex=None):
    """Import the network for testing. \n
    Input :
        begin (int) : time of the begining of the simulation
        end (int) : time of the end of the simulaiton
        rho (int) : penalty factor of the ADMM
        flex (int, None) : Flexibility of agent"""
    panda_network = pp.from_pickle(os.path.join(absolute_dirpath,"../../test_case/use/case_CIGRE_lv_test.p"))
    # Change line limits
    panda_network.line.max_i_ka *= 0.5
    # Set max loading percent
    panda_network.line["max_loading_percent"] = 100
    
    ### Simulation : set the time interval of the simulation
    panda_network.data = panda_network.data[panda_network.data["time"]<end]
    panda_network.data = panda_network.data[panda_network.data["time"]>=begin]
    panda_network.data.time -= begin # Index has to strat at 0
    
    ### Set agent
    # Ext_grid fleibility
    panda_network.flex_data.flexibility[0] = 10
    # Agent flexibility so that the grid price is around something usual (20€/MWh)
    panda_network.flex_data.flexibility *= 0.01
    if flex is not None :
        panda_network.flex_data.flexibility = flex
    # Number of agentn, ext grid is counted as an agent
    n_agent = len(panda_network.load)+1 
    
    ### Init classes
    So = SO(n_agent,
            grid_cost=0,
            strategy="PI",
            Kp=0.01,
            Ki=0.01,
            Kd=0,
            line_limits=panda_network.line["max_loading_percent"][0],
            grid_cost_type = "uniq")
    communication = Communication(n_agent, grid_cost=So.grid_cost)
    ADMM = Admm(panda_network, So, communication, rho=rho, compute_opf=2)
    
    return ADMM

def test_admm_optimum():
    """Test that the optimum found by the ADMM (when it has enougth iteration to converge) is the same
    panda power OPF."""
    ADMM = import_net(begin=1150, end = 1152)
    ADMM.run(warm_start=True, max_it=100, plot=False)
    ADMM.memory_warm.compute_residual()
    # Check that residual are below a certain threshold
    assert ADMM.memory_warm.prim_res[-1] < 1e-3
    assert ADMM.memory_warm.dual_res[-1] < 1e-3
    # Check that the results of the OPF and Exogenous are similar
    dif = np.sum(np.abs(ADMM.memory_warm.P_exchanged_opf[-1] - ADMM.memory_warm.P_exchanged[-1]))
    assert dif < 0.5
    
def test_setters():
    """Test that setters modifie all attributes correctly"""
    # Check that the rho is modified as exepted
    rho = 800
    ADMM = import_net(begin=1000, end = 1002, rho=rho)
    for ag in ADMM.agents:
        assert ag.rho == rho
    # Check flexibility
    flex = 20
    ADMM = import_net(begin=1000, end = 1002, flex=flex)
    for ag in ADMM.agents:
        assert ag.flex == flex

def test_plot_memory():
    """Test that the time flow correctly and that all variable are updated as expected"""
    ADMM = import_net(begin=1150, end = 1155)
    ADMM.SO.strategy = "None"
    try :
        ADMM.run(warm_start=True, max_it=50, plot=True)
        ADMM.memory.plot_convergence(scale = True)
        ADMM.memory.plot_convergence(scale = False)
        ADMM.memory.plot_agent_power_during_simu(list_agent=None, legend=False)
        ADMM.memory.plot_agent_power_during_simu(list_agent=[1,3], legend=True)
        ADMM.memory.plot_compare_to_OPF_simu(ID_agent=5, legend=False, objective=False, ax=None)
        ADMM.memory.plot_compare_to_OPF_simu(ID_agent=5, legend=True, objective=True, ax=None)
        ADMM.memory.plot_line_loading(t=0)
        ADMM.memory.plot_cost_evolution()
        ADMM.memory.plot_max_loading_percent()
        ADMM.memory.plot_all_agent_power(t=0, warm=False)
        ADMM.memory.plot_all_agent_power(t=2, warm=True)
        ADMM.memory.plot_cost_objective_function(t=0)
        ADMM.memory.plot_flexibility()
        ADMM.memory.plot_fig1(10, 5)
        ADMM.memory.plot_cost_vs_flex(10,5)
        ADMM.memory.plot_overload_histogram()
    except :
        assert False, "One of the plot did not work"
    
    try :
        Ki = np.load(os.path.join(absolute_dirpath,"../../figures/ISGT/Ki.npy"))
        Kp = np.load(os.path.join(absolute_dirpath,"../../figures/ISGT/Kp.npy"))
        Stats_power = np.load(os.path.join(absolute_dirpath,"../../figures/ISGT/Stats_power.npy"))
        Stats_lines = np.load(os.path.join(absolute_dirpath,"../../figures/ISGT/Stats_lines.npy"))
        
        # Plot
        ADMM.memory.PI_sensi_analysis(Stats_power, Stats_lines, Ki, Kp, custom_label=True)
    except :
        assert False, "PI_sensi_analysis does not work"













