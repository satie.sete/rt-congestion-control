# -*- coding: utf-8 -*-
"""
Created on Mon Apr 26 10:39:36 2021

@author: Guenole
"""
import numpy as np

import pickle 

import os
absolute_dirpath = os.path.abspath(os.path.dirname(__file__))

########################################################################################################
############################################ Import files ##############################################
########################################################################################################

# Import main results
file = open(os.path.join(absolute_dirpath,'results_ISGT.obj'), 'rb') 
ADMM = pickle.load(file)

# Import sensitivity analysis results
Ki = np.load(os.path.join(absolute_dirpath,"Ki.npy"))
Kp = np.load(os.path.join(absolute_dirpath,"Kp.npy"))
Stats_power = np.load(os.path.join(absolute_dirpath,"Stats_power.npy"))
Stats_lines = np.load(os.path.join(absolute_dirpath,"Stats_lines.npy"))

########################################################################################################
################################################ PLOT ##################################################
########################################################################################################
ADMM.memory.plot_fig1(10, 5)
ADMM.memory.plot_convergence(scale = True)
ADMM.memory.plot_cost_vs_flex(10,5)
ADMM.memory.plot_overload_histogram()



ADMM.memory.PI_sensi_analysis(Stats_power, Stats_lines, Ki, Kp, custom_label=True)