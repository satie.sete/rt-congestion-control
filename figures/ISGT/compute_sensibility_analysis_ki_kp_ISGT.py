# -*- coding: utf-8 -*-
"""
Created on Tue Jan 12 14:09:49 2021

@author: Guenole CHEROT
"""

import pandapower as pp
import pandapower.networks
import warnings
warnings.filterwarnings("ignore")

from rt_congestion_control.so import SO
from rt_congestion_control.communication import Communication
from rt_congestion_control.admm import Admm

import numpy as np
import os
absolute_dirpath = os.path.abspath(os.path.dirname(__file__))

########################################################################################################
########################################### Initialisation #############################################
########################################################################################################

# Import data
panda_network = pp.from_pickle(os.path.join(absolute_dirpath,"../../test_case/use/case_CIGRE_lv_pecan.p"))
panda_network.line.max_i_ka *= 0.5
panda_network.line["max_loading_percent"] = 100
begin = 1100
end = 1439
panda_network.data = panda_network.data[panda_network.data["time"]<end]
panda_network.data = panda_network.data[panda_network.data["time"]>=begin]
panda_network.data.time -= begin
panda_network.flex_data.flexibility[0] = 10
ratio = 100
panda_network.flex_data.flexibility *= 1/ratio

n_agent = len(panda_network.load)+1 # ext grid is counted as an agent

########################################################################################################
################################################ LOOP ##################################################
########################################################################################################
p = np.logspace(-3,-1,9)
Kp = np.concatenate(([0],p))
i = np.logspace(-3,-1,13)
Ki = np.concatenate(([0],i))

Stats_power = np.zeros((np.size(Kp),np.size(Ki),2))
Stats_lines = np.zeros((np.size(Kp),np.size(Ki),2))

for i in range(np.size(Kp)):
    for j in range(np.size(Ki)):
            So = SO(n_agent,
                    grid_cost=0,
                    strategy="PI",
                    Kp=Kp[i],
                    Ki=Ki[j],
                    Kd=0,
                    line_limits=panda_network.line["max_loading_percent"][0],
                    grid_cost_type = "uniq")
            
            communication = Communication(n_agent, grid_cost=So.grid_cost)
            ADMM = Admm(panda_network, So, communication,compute_opf=1, rho=70000/ratio)
            
            # Run
            ADMM.run(warm_start=True, max_it=100, plot=False)
            
            # Save results
            Stats_power[i,j] = ADMM.memory.stats_power()
            Stats_lines[i,j] = ADMM.memory.stats_lines()

# Clean data
copy_Stats_power = np.nan_to_num(Stats_power,copy=True, nan=0)
np.nan_to_num(Stats_power,copy=False, nan=np.max(copy_Stats_power[:,:,1:]))

# Save data
np.save(os.path.join(absolute_dirpath,"Ki"), Ki)
np.save(os.path.join(absolute_dirpath,"Kp"), Kp)
np.save(os.path.join(absolute_dirpath,"Stats_power"), Stats_power)
np.save(os.path.join(absolute_dirpath,"Stats_lines"), Stats_lines)